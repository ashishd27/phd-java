package com.phd.assessmentTool.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.phd.assessmentTool.model.*;
import com.phd.assessmentTool.model.Enums.*;
import com.phd.assessmentTool.repository.QuestionsRepository;
import com.phd.assessmentTool.repository.UserGuidanceVideosRepository;
import com.phd.assessmentTool.repository.UserQuestionAnswerRepository;
import com.phd.assessmentTool.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

@Service
public class AssessmentToolService {

    @Autowired
    QuestionsRepository questionsRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserQuestionAnswerRepository userQuestionAnswerRepository;
    @Autowired
    UserGuidanceVideosRepository userGuidanceVideosRepository;
    public static int MODULE_1_COUNT_LIMIT = 1;
    public static int MODULE_2_COUNT_LIMIT = 2;


    public JsonNode getQuestions(ModuleEnum moduleType, User userDetail){

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        ArrayNode arrayNode = null;
                AgeEnum ageCategory = getAgeCategory(userDetail.getGrade());
        if(moduleType == ModuleEnum.MODULE_1) {
            arrayNode =
            objectMapper.convertValue(questionsRepository.findByAgeCategoryAndModuleTypeOrderByOrderAsc(AgeEnum.AGE_ALL, moduleType), ArrayNode.class);
            objectNode.put("questions", arrayNode);
              if (userDetail.getModule1AttemptCount() == null) {
                  objectNode.put("attempt", "0/1");
              } else {
                  objectNode.put("attempt", userDetail.getModule1AttemptCount() + "/" + MODULE_1_COUNT_LIMIT);
              }

        } else if (moduleType == ModuleEnum.MODULE_2) {
            arrayNode =  objectMapper.convertValue(questionsRepository.findByAgeCategoryAndModuleTypeOrderByOrderAsc(ageCategory, moduleType), ArrayNode.class);
            objectNode.put("questions", arrayNode);
            if (userDetail.getModule2AttemptCount() == null) {
                objectNode.put("attempt", "0/2");
            } else {
                objectNode.put("attempt", userDetail.getModule2AttemptCount() + "/" + MODULE_2_COUNT_LIMIT);
            }
        }

        return objectNode;
    }

    public Iterable<Questions> createQuestions(List<Questions> questionsList){
        return questionsRepository.saveAll(questionsList);

    }
    public Iterable<UserGuidanceVideos> addVideos(List<UserGuidanceVideos> userGuidanceVideos){
        return userGuidanceVideosRepository.saveAll(userGuidanceVideos);
    }

    public ResponseEntity<?> getVideos(User user, ModuleEnum moduleType){

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode videoArrayNode = objectMapper.createArrayNode();
        List<UserQuestionAnswer> userQuestionAnswerList =
     userQuestionAnswerRepository.findByUserAndUserAttemptAndModuleType(user, 1, moduleType);

        if (userQuestionAnswerList == null || userQuestionAnswerList.size() == 0) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Please submit module 2 test first , in order to watch videos");

        }


        List<UserQuestionAnswer> userQuestionIncorrectAnswerList =
                userQuestionAnswerList.stream().filter(obj -> !obj.getCorrect()).collect(Collectors.toList());

        List<UserGuidanceVideos> userGuidanceVideosList =  userGuidanceVideosRepository
                .findByAgeCategoryAndModuleType(getAgeCategory(user.getGrade()), moduleType);

        if (userGuidanceVideosList == null || userGuidanceVideosList.size() == 0) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("No video found");

        }
        List<String> videoList = new ArrayList<>();
        userQuestionIncorrectAnswerList.stream().forEach(obj -> {
            Optional<UserGuidanceVideos> incorrectAnswerVideo =
            userGuidanceVideosList.stream().filter(val -> val.getModule2QuestionCategory() ==
                    obj.getQuestion().getModule2QuestionCategory()).findFirst();

            if (incorrectAnswerVideo.isPresent() && incorrectAnswerVideo != null) {
                ObjectNode objectNode = objectMapper.createObjectNode();
                objectNode.put("ageType" ,  obj.getQuestion().getAgeCategory().toString());
                objectNode.put("moduleType" ,  obj.getModuleType().toString());
                objectNode.put("questionCategory" ,  obj.getQuestion().getModule2QuestionCategory().toString());
                objectNode.put("videoUrl" ,  incorrectAnswerVideo.get().getVideoUrl());
                objectNode.put("videoTitle" ,  incorrectAnswerVideo.get().getVideoTitle());
                videoArrayNode.add(objectNode);
            }
        });
        return ResponseEntity.status(HttpStatus.OK).body(videoArrayNode);
    }

    public String submitQuestionAnswer(List<UserAnswerModel> userAnswerModel, String userName, ModuleEnum moduleType){


        User userDetail = userRepository.findByUsername(userName);
        List<UserQuestionAnswer> userQuestionAnswerList = new ArrayList<>();


        if (moduleType.equals(ModuleEnum.MODULE_1)) {
            if (userDetail.getModule1AttemptCount() != null && userDetail.getModule1AttemptCount() == MODULE_1_COUNT_LIMIT) {
                return "Max Attempt reached , cannot submit again";
            }

            if (userDetail.getModule1AttemptCount() == null) {
                userDetail.setModule1AttemptCount(1);
            } else {
                userDetail.setModule1AttemptCount(userDetail.getModule1AttemptCount() + 1);
            }
        } else if (moduleType.equals(ModuleEnum.MODULE_2)) {
            if (userDetail.getModule2AttemptCount() != null && userDetail.getModule2AttemptCount() == MODULE_2_COUNT_LIMIT) {
                return "Max Attempt reached , cannot submit again";
            }

            if (userDetail.getModule2AttemptCount() == null) {
                userDetail.setModule2AttemptCount(1);
            } else {
                userDetail.setModule2AttemptCount(userDetail.getModule2AttemptCount() + 1);
            }
        }
            userAnswerModel.stream().forEach(obj -> {
                UserQuestionAnswer userQuestionAnswer = new UserQuestionAnswer();
                if (moduleType.equals(ModuleEnum.MODULE_1)) {
                    userQuestionAnswer.setUserAttempt(userDetail.getModule1AttemptCount());

                } else if (moduleType.equals(ModuleEnum.MODULE_2)) {
                    userQuestionAnswer.setUserAttempt(userDetail.getModule2AttemptCount());

                }
                userQuestionAnswer.setAnswer(obj.getAnswer());
                userQuestionAnswer.setQuestion(obj.getQuestion());
                userQuestionAnswer.setUser(userDetail);
                userQuestionAnswer.setModuleType(moduleType);
                userQuestionAnswer.setCorrect(checkAnswerIsCorrectOrNot(
                        obj.getQuestion().getQuestionAnswer().getAnswer(), obj.getAnswer(), obj.getQuestion().getQuestionType()));
                userQuestionAnswer.setTimeTaken(obj.getTimeTaken());
                userQuestionAnswerList.add(userQuestionAnswer);
            });


        userQuestionAnswerRepository.saveAll(userQuestionAnswerList);

        return "Submitted SuccessFully";

    }

    public boolean checkAnswerIsCorrectOrNot(String questionAnswer, String userAnswer, QuestionType questionType) {

        if (questionType == QuestionType.STRING_TYPE) {
            return questionAnswer.equalsIgnoreCase(userAnswer);
            } else if (questionType == QuestionType.INTEGER_TYPE) {
            return Integer.parseInt(questionAnswer) == Integer.parseInt(userAnswer);
        }
        return false;
    }

    public AgeEnum getAgeCategory(String grade) {
        if (grade.equals("7")) {
            return AgeEnum.AGE_SEVEN;
        } else if (grade.equals("8")) {
            return AgeEnum.AGE_EIGHT;
        } else {
            return AgeEnum.AGE_SIX;
        }
    }

    public ResponseEntity<?> getAnalyticsData1(User userDetail, ModuleEnum moduleType, int attempt) {

        List<Object> dataList =  new ArrayList<>();
        Map<String, Object> dataMap = new HashMap<>();
        if(moduleType == ModuleEnum.MODULE_1) {
            List<UserQuestionAnswer> module1Answers =
                    userQuestionAnswerRepository.findByUserAndModuleType(userDetail, moduleType);
            if(module1Answers == null || module1Answers.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("No Data Found");
            }
            Map<Boolean, Long> correctVsIncorrectCountMap = module1Answers.stream()
                    .collect(Collectors.groupingBy(UserQuestionAnswer::getCorrect, counting()));

            Map<Module1QuestionCategoryEnum, List<UserQuestionAnswer>> categoryWiseResultMap =
            module1Answers.stream()
                    .collect(Collectors.groupingBy(obj -> obj.getQuestion().getModule1QuestionCategory()));
            Map<Module1QuestionCategoryEnum, Object> categoryWiseResult = new HashMap<>();
            categoryWiseResultMap.forEach((key, value) -> {
                Map<String, Integer> dataValueMap = new HashMap<>();
                dataValueMap.put("correct", value.stream().filter(obj -> obj.getCorrect() == true).collect(Collectors.toList()).size());
                dataValueMap.put("inCorrect", value.stream().filter(obj -> obj.getCorrect() == false).collect(Collectors.toList()).size());
                categoryWiseResult.put(key, dataValueMap);
            });
            dataMap.put("userAnswerDetail", module1Answers);
            dataMap.put("totalQuestionAnlysis", correctVsIncorrectCountMap);
            dataMap.put("categoryWiseAnalysis", categoryWiseResult);


        } else if (moduleType == ModuleEnum.MODULE_2) {
            List<UserQuestionAnswer> module2Answers =
                    userQuestionAnswerRepository.findByUserAndUserAttemptAndModuleType(userDetail, attempt,moduleType);
            if(module2Answers == null || module2Answers.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("No Data Found");
            }

            Map<Boolean, Long> correctVsIncorrectCountMap = module2Answers.stream()
                    .collect(Collectors.groupingBy(UserQuestionAnswer::getCorrect, counting()));

            Map<Module2QuestionCategoryEnum, List<UserQuestionAnswer>> categoryWiseResultMap =
                    module2Answers.stream()
                            .collect(Collectors.groupingBy(obj -> obj.getQuestion().getModule2QuestionCategory()));
            Map<Module2QuestionCategoryEnum, Object> categoryWiseResult = new HashMap<>();
            categoryWiseResultMap.forEach((key, value) -> {
                Map<String, Integer> dataValueMap = new HashMap<>();
                dataValueMap.put("correct", value.stream().filter(obj -> obj.getCorrect() == true).collect(Collectors.toList()).size());
                dataValueMap.put("inCorrect", value.stream().filter(obj -> obj.getCorrect() == false).collect(Collectors.toList()).size());
                categoryWiseResult.put(key, dataValueMap);
            });
            dataMap.put("userAnswerDetail", module2Answers);
            dataMap.put("totalQuestionAnlysis", correctVsIncorrectCountMap);
            dataMap.put("categoryWiseAnalysis", categoryWiseResult);

        }
        return ResponseEntity.status(HttpStatus.OK).body(dataMap);
    }


    public ResponseEntity<?> getAnalyticsData(User userDetail) {

        List<Object> dataList =  new ArrayList<>();
        Map<String, Object> dataMap = new HashMap<>();

            List<UserQuestionAnswer> module1Answers =
                    userQuestionAnswerRepository.findByUserAndModuleType(userDetail, ModuleEnum.MODULE_1);
            List<UserQuestionAnswer> module2Answers =
                userQuestionAnswerRepository.findByUserAndUserAttemptAndModuleType(userDetail, 1,ModuleEnum.MODULE_2);
        List<UserQuestionAnswer> module2AnswersAttempt2 =
                userQuestionAnswerRepository.findByUserAndUserAttemptAndModuleType(userDetail, 2,ModuleEnum.MODULE_2);


        if(module1Answers != null && !module1Answers.isEmpty()) {

            Map<Boolean, Long> correctVsIncorrectCountMap = module1Answers.stream()
                    .collect(Collectors.groupingBy(UserQuestionAnswer::getCorrect, counting()));

            Map<Module1QuestionCategoryEnum, List<UserQuestionAnswer>> categoryWiseResultMap =
                    module1Answers.stream()
                            .collect(Collectors.groupingBy(obj -> obj.getQuestion().getModule1QuestionCategory()));
            Map<Module1QuestionCategoryEnum, Object> categoryWiseResult = new HashMap<>();
            categoryWiseResultMap.forEach((key, value) -> {
                Map<String, Integer> dataValueMap = new HashMap<>();
                dataValueMap.put("correct", value.stream().filter(obj -> obj.getCorrect() == true).collect(Collectors.toList()).size());
                dataValueMap.put("inCorrect", value.stream().filter(obj -> obj.getCorrect() == false).collect(Collectors.toList()).size());
                categoryWiseResult.put(key, dataValueMap);
            });
            //dataMap.put("userAnswerDetail", module1Answers);
            dataMap.put("module1totalQuestionAnalysis", correctVsIncorrectCountMap);
            dataMap.put("module1categoryWiseAnalysis", categoryWiseResult);


        }
        if (module2Answers != null && !module2Answers.isEmpty()) {

            Map<Boolean, Long> correctVsIncorrectCountMap = module2Answers.stream()
                    .collect(Collectors.groupingBy(UserQuestionAnswer::getCorrect, counting()));

            Map<Module2QuestionCategoryEnum, List<UserQuestionAnswer>> categoryWiseResultMap =
                    module2Answers.stream()
                            .collect(Collectors.groupingBy(obj -> obj.getQuestion().getModule2QuestionCategory()));
            Map<Module2QuestionCategoryEnum, Object> categoryWiseResult = new HashMap<>();
            categoryWiseResultMap.forEach((key, value) -> {
                Map<String, Integer> dataValueMap = new HashMap<>();
                dataValueMap.put("correct", value.stream().filter(obj -> obj.getCorrect() == true).collect(Collectors.toList()).size());
                dataValueMap.put("inCorrect", value.stream().filter(obj -> obj.getCorrect() == false).collect(Collectors.toList()).size());
                categoryWiseResult.put(key, dataValueMap);
            });
            //dataMap.put("userAnswerDetail", module2Answers);
            dataMap.put("module2Attempt1TotalQuestionAnalysis", correctVsIncorrectCountMap);
            dataMap.put("module2Attempt1CategoryWiseAnalysis", categoryWiseResult);

        }
        if (module2AnswersAttempt2 != null && !module2AnswersAttempt2.isEmpty()) {

            Map<Boolean, Long> correctVsIncorrectCountMap = module2AnswersAttempt2.stream()
                    .collect(Collectors.groupingBy(UserQuestionAnswer::getCorrect, counting()));

            Map<Module2QuestionCategoryEnum, List<UserQuestionAnswer>> categoryWiseResultMap =
                    module2AnswersAttempt2.stream()
                            .collect(Collectors.groupingBy(obj -> obj.getQuestion().getModule2QuestionCategory()));
            Map<Module2QuestionCategoryEnum, Object> categoryWiseResult = new HashMap<>();
            categoryWiseResultMap.forEach((key, value) -> {
                Map<String, Integer> dataValueMap = new HashMap<>();
                dataValueMap.put("correct", value.stream().filter(obj -> obj.getCorrect() == true).collect(Collectors.toList()).size());
                dataValueMap.put("inCorrect", value.stream().filter(obj -> obj.getCorrect() == false).collect(Collectors.toList()).size());
                categoryWiseResult.put(key, dataValueMap);
            });
            //dataMap.put("userAnswerDetail", module2Answers);
            dataMap.put("module2Attempt2TotalQuestionAnalysis", correctVsIncorrectCountMap);
            dataMap.put("module2Attempt2CategoryWiseAnalysis", categoryWiseResult);

        }
        if (module2Answers != null && !module2Answers.isEmpty() &&
        module2AnswersAttempt2 != null && !module2AnswersAttempt2.isEmpty()) {


        }

        return ResponseEntity.status(HttpStatus.OK).body(dataMap);
    }


}
