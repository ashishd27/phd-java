package com.phd.assessmentTool.controller;

import com.phd.assessmentTool.config.JwtRequestFilter;
import com.phd.assessmentTool.model.*;
import com.phd.assessmentTool.model.Enums.ModuleEnum;
import com.phd.assessmentTool.repository.UserRepository;
import com.phd.assessmentTool.service.AssessmentToolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AssessmentToolController {

    @Autowired
    AssessmentToolService assessmentToolService;
    @Autowired
    UserRepository userRepository;

    public static int MODULE_1_COUNT_LIMIT = 1;
    public static int MODULE_2_COUNT_LIMIT = 2;


    @RequestMapping(value = "/get-questions", method = RequestMethod.GET)
    public ResponseEntity<?> getQuestions(@RequestParam(name = "moduleType") String moduleType) throws Exception
    {
        User userDetail = userRepository.findByUsername(JwtRequestFilter.getLoggedInUserName());
        if (userDetail == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid user");

        }
        if (ModuleEnum.valueOf(moduleType) == ModuleEnum.MODULE_1 &&
                userDetail.getModule1AttemptCount() != null &&
                userDetail.getModule1AttemptCount()  == MODULE_1_COUNT_LIMIT) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("User has already submitted this test , please goto result section");

        } else if (ModuleEnum.valueOf(moduleType) == ModuleEnum.MODULE_2 &&
                userDetail.getModule2AttemptCount() != null &&
                userDetail.getModule2AttemptCount()  == MODULE_2_COUNT_LIMIT) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("User has already submitted this test , please goto result section");

        }

        return ResponseEntity.ok(assessmentToolService.getQuestions(ModuleEnum.valueOf(moduleType), userDetail));
    }


    @RequestMapping(value = "/create-questions", method = RequestMethod.POST)
    public ResponseEntity<?> createQuestions(@RequestBody List<Questions> questionsList) throws Exception
    {
        return  ResponseEntity.ok(assessmentToolService.createQuestions(questionsList));
    }

    @RequestMapping(value = "/submit-question-answer", method = RequestMethod.POST)
    public ResponseEntity<?> submitQuestionAnswer(@RequestBody List<UserAnswerModel> userAnswerModel,
                                                  @RequestParam(name = "moduleType") String moduleType) throws Exception
    {
        return  ResponseEntity.ok(assessmentToolService.submitQuestionAnswer(userAnswerModel,
                JwtRequestFilter.getLoggedInUserName(), ModuleEnum.valueOf(moduleType)));
    }

    @RequestMapping(value = "/add-videos", method = RequestMethod.POST)
    public ResponseEntity<?> addVideos(@RequestBody List<UserGuidanceVideos> userGuidanceVideos) throws Exception
    {
        return  ResponseEntity.ok(assessmentToolService.addVideos(userGuidanceVideos));
    }

    @RequestMapping(value = "/get-videos", method = RequestMethod.POST)
    public ResponseEntity<?> getVideos(@RequestParam(name = "moduleType") String moduleType) throws Exception
    {
        User userDetail = userRepository.findByUsername(JwtRequestFilter.getLoggedInUserName());
        if (userDetail == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid user");

        }
        return  assessmentToolService.getVideos(userDetail, ModuleEnum.valueOf(moduleType));
    }

    @RequestMapping(value = "/get-analytics-data", method = RequestMethod.GET)
    public ResponseEntity<?> getAnalyticsData() throws Exception
    {
        User userDetail = userRepository.findByUsername(JwtRequestFilter.getLoggedInUserName());
        if (userDetail == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Invalid user");

        }
        return  assessmentToolService.getAnalyticsData(userDetail);
    }



}
