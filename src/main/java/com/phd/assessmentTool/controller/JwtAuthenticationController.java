package com.phd.assessmentTool.controller;

import com.phd.assessmentTool.config.JwtTokenUtil;
import com.phd.assessmentTool.config.WebSecurityConfig;
import com.phd.assessmentTool.model.JwtRequest;
import com.phd.assessmentTool.model.JwtResponse;
import com.phd.assessmentTool.model.JwtUserDetails;
import com.phd.assessmentTool.model.User;
import com.phd.assessmentTool.repository.UserRepository;
import com.phd.assessmentTool.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception
    {
        authenticate(authenticationRequest.getUsername(),authenticationRequest.getPassword());
        final UserDetails userDetails =
                userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody User userBody) throws Exception
    {
        userBody.setPassword(webSecurityConfig.passwordEncoder().encode(userBody.getPassword()));
        if (userRepository.findByUsername(userBody.getUsername()) == null) {
            userBody.setModule1AttemptCount(0);
            userBody.setModule2AttemptCount(0);
            return ResponseEntity.ok(userRepository.save(userBody));

        }
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("User Name Already Taken");

    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
