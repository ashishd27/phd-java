package com.phd.assessmentTool.repository;

import com.phd.assessmentTool.model.Enums.AgeEnum;
import com.phd.assessmentTool.model.Enums.ModuleEnum;
import com.phd.assessmentTool.model.Questions;
import com.phd.assessmentTool.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionsRepository extends CrudRepository<Questions, Long> {

    List<Questions> findByAgeCategoryAndModuleTypeOrderByOrderAsc(AgeEnum ageCategory, ModuleEnum moduleType);
}
