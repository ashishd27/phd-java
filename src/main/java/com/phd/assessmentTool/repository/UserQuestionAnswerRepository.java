package com.phd.assessmentTool.repository;

import com.phd.assessmentTool.model.Enums.ModuleEnum;
import com.phd.assessmentTool.model.User;
import com.phd.assessmentTool.model.UserQuestionAnswer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@EnableJpaRepositories
@Repository
public interface UserQuestionAnswerRepository extends CrudRepository<UserQuestionAnswer, Long> {

    List<UserQuestionAnswer> findByUserAndUserAttemptAndModuleType(User user, int userAttempt, ModuleEnum moduleTypes);
    List<UserQuestionAnswer> findByUserAndModuleType(User user, ModuleEnum moduleTypes);


}
