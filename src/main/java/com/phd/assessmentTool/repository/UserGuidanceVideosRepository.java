package com.phd.assessmentTool.repository;

import com.phd.assessmentTool.model.Enums.AgeEnum;
import com.phd.assessmentTool.model.Enums.ModuleEnum;
import com.phd.assessmentTool.model.UserGuidanceVideos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGuidanceVideosRepository  extends CrudRepository<UserGuidanceVideos, Long> {

    List<UserGuidanceVideos> findByAgeCategoryAndModuleType(AgeEnum ageEnum, ModuleEnum moduleEnum);
}
