package com.phd.assessmentTool.model.Enums;

public enum AgeEnum {
    AGE_ALL(0),
    AGE_SIX(6),
    AGE_SEVEN(7),
    AGE_EIGHT(8);
    private Integer val;

    AgeEnum(Integer val) {
        this.val = val;
    }

    public Integer getVal() {
        return val;
    }


}
