package com.phd.assessmentTool.model.Enums;

public enum QuestionType {

    STRING_TYPE(1),
    INTEGER_TYPE(2);
    private Integer val;

    QuestionType(Integer val) {
        this.val = val;
    }

    public Integer getVal() {
        return val;
    }
}
