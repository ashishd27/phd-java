package com.phd.assessmentTool.model.Enums;

public enum Module1QuestionCategoryEnum {

    READING(1),
    WRITING(2),
    MATHEMATICS(3),
    ATTENTION(4),
    EXECUTIVE_FUNCTIONALITY(5),
    ORAL(6),
    MEMORY(7),
    SOCIAL(8),
    MOTOR(9);

    private Integer val;

    Module1QuestionCategoryEnum(Integer val) {
        this.val = val;
    }

    public Integer getVal() {
        return val;
    }
}
