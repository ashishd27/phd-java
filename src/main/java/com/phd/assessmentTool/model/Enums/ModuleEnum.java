package com.phd.assessmentTool.model.Enums;

public enum ModuleEnum {

    MODULE_1(1),
    MODULE_2(2);
    private Integer val;

    ModuleEnum(Integer val) {
        this.val = val;
    }

    public Integer getVal() {
        return val;
    }

}
