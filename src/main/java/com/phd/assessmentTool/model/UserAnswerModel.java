package com.phd.assessmentTool.model;

import com.phd.assessmentTool.model.Enums.ModuleEnum;

public class UserAnswerModel {

    Questions question;
    long timeTaken;
    String answer;



    public Questions getQuestion() {
        return question;
    }

    public void setQuestion(Questions question) {
        this.question = question;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
