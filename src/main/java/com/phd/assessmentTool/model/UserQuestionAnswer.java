package com.phd.assessmentTool.model;


import com.phd.assessmentTool.model.Enums.ModuleEnum;

import javax.persistence.*;

@Entity
@Table(name = "user_question_answer")
public class UserQuestionAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @OneToOne
    Questions question;

    @Column(name = "time_taken")
    long timeTaken;

    @Column(name = "answer")
    String answer;

    @Column(name = "is_correct")
    Boolean isCorrect;

    @Column(name = "user_attempt")
    int userAttempt;



    public void setUserAttempt(int userAttempt) {
        this.userAttempt = userAttempt;
    }

    public ModuleEnum getModuleType() {
        return moduleType;
    }

    public int getUserAttempt() {
        return userAttempt;
    }
    public void setModuleType(ModuleEnum moduleType) {
        this.moduleType = moduleType;
    }

    @Column(name = "module_type")
    ModuleEnum moduleType;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @ManyToOne
    User user;

    @Column(name = "level")
    int level;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Questions getQuestion() {
        return question;
    }

    public void setQuestion(Questions question) {
        this.question = question;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
