package com.phd.assessmentTool.model;



import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_name")
    String username;
    @Column(name = "password")
    String password;
    @Column(name = "gender")
    String gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "dob")
    Date dob;
    @Column(name = "mother_tongue")
    String motherTongue;
    @Column(name = "contact_no")
    String contactNo;
    @Column(name = "grade")
    String grade;
    @Column(name = "medium_of_instruction")
    String mediumOfInstruction;
    @Column(name = "parent_education")
    String parentEducation;

    @Column(name = "parent_annual_income")
    String parentAnnualIncome;
    @Column(name = "active")
    Boolean active;
    @Column(name = "age")
    Integer age;
    @Column(name = "module1_attempt_count", columnDefinition = "integer default 0")
    Integer module1AttemptCount;
    @Column(name = "module2_attempt_count", columnDefinition = "integer default 0")
    Integer module2AttemptCount;

    public Integer getModule1AttemptCount() {
        return module1AttemptCount;
    }

    public void setModule1AttemptCount(Integer module1AttemptCount) {
        this.module1AttemptCount = module1AttemptCount;
    }

    public Integer getModule2AttemptCount() {
        return module2AttemptCount;
    }

    public void setModule2AttemptCount(Integer module2AttemptCount) {
        this.module2AttemptCount = module2AttemptCount;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getMotherTongue() {
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getMediumOfInstruction() {
        return mediumOfInstruction;
    }

    public void setMediumOfInstruction(String mediumOfInstruction) {
        this.mediumOfInstruction = mediumOfInstruction;
    }

    public String getParentEducation() {
        return parentEducation;
    }

    public void setParentEducation(String parentEducation) {
        this.parentEducation = parentEducation;
    }

    public String getParentAnnualIncome() {
        return parentAnnualIncome;
    }

    public void setParentAnnualIncome(String parentAnnualIncome) {
        this.parentAnnualIncome = parentAnnualIncome;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
