package com.phd.assessmentTool.model;

import com.phd.assessmentTool.model.Enums.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "questions")
public class Questions {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "question")
    String question;

    @Column(name = "has_image")
    Boolean hasImage;

    @Column(name = "image_url")
    String imageUrl;

    @Column(name = "module_type")
    ModuleEnum moduleType;

    @Column(name = "age_category")
    AgeEnum ageCategory;



    @Nullable
    @Column(name = "Module1_question_category")
    Module1QuestionCategoryEnum module1QuestionCategory;

    @Nullable
    @Column(name = "Module2_question_category")
    Module2QuestionCategoryEnum module2QuestionCategory;

    @Column(name = "question_order")
    int order;

    @Column(name = "level")
    int level;


    String attempt;

    @OneToMany(cascade = CascadeType.ALL)
    List<QuestionChoices> questionChoicesList;

    @OneToOne(cascade = CascadeType.ALL)
    QuestionAnswers questionAnswer;

    @Column(name = "question_type")
    QuestionType questionType;

    public int getLevel() {
        return level;
    }

    @Nullable
    public Module2QuestionCategoryEnum getModule2QuestionCategory() {
        return module2QuestionCategory;
    }

    public void setModule2QuestionCategory(@Nullable Module2QuestionCategoryEnum module2QuestionCategory) {
        this.module2QuestionCategory = module2QuestionCategory;
    }

    public String getAttempt() {
        return attempt;
    }

    public void setAttempt(String attempt) {
        this.attempt = attempt;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public QuestionAnswers getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(QuestionAnswers questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public ModuleEnum getModuleType() {
        return moduleType;
    }

    public void setModuleType(ModuleEnum moduleType) {
        this.moduleType = moduleType;
    }

    public AgeEnum getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(AgeEnum ageCategory) {
        this.ageCategory = ageCategory;
    }

    public Module1QuestionCategoryEnum getModule1QuestionCategory() {
        return module1QuestionCategory;
    }

    public void setModule1QuestionCategory(Module1QuestionCategoryEnum module1QuestionCategory) {
        this.module1QuestionCategory = module1QuestionCategory;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<QuestionChoices> getQuestionChoicesList() {
        return questionChoicesList;
    }

    public void setQuestionChoicesList(List<QuestionChoices> questionChoicesList) {
        this.questionChoicesList = questionChoicesList;
    }

}
