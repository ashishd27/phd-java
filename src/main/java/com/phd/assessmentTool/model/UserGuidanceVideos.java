package com.phd.assessmentTool.model;

import com.phd.assessmentTool.model.Enums.AgeEnum;
import com.phd.assessmentTool.model.Enums.Module2QuestionCategoryEnum;
import com.phd.assessmentTool.model.Enums.ModuleEnum;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "user_guidance_video")
public class UserGuidanceVideos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "video_url")
    String videoUrl;

    @Column(name = "video_title")
    String videoTitle;

    @Column(name = "module_type")
    ModuleEnum moduleType;

    @Column(name = "age_category")
    AgeEnum ageCategory;

    @Nullable
    @Column(name = "module2_question_category")
    Module2QuestionCategoryEnum module2QuestionCategory;

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public int getId() {
        return id;
    }


    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public ModuleEnum getModuleType() {
        return moduleType;
    }

    public void setModuleType(ModuleEnum moduleType) {
        this.moduleType = moduleType;
    }

    public AgeEnum getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(AgeEnum ageCategory) {
        this.ageCategory = ageCategory;
    }

    @Nullable
    public Module2QuestionCategoryEnum getModule2QuestionCategory() {
        return module2QuestionCategory;
    }

    public void setModule2QuestionCategory(@Nullable Module2QuestionCategoryEnum module2QuestionCategory) {
        this.module2QuestionCategory = module2QuestionCategory;
    }
}
