package com.phd.assessmentTool.model;

import javax.persistence.*;

@Entity
@Table(name = "question_choices")
public class QuestionChoices {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "choice")
    String choice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }
}
